import requests
import json
import sys

if sys.argv[1].isdecimal():
    url = "http://site.api.espn.com/apis/site/v2/sports/football/college-football/scoreboard?groups=80&limit=100&week=" + sys.argv[1]
else:
    url = "http://site.api.espn.com/apis/site/v2/sports/football/college-football/scoreboard?groups=80&limit=100"
scores = requests.get(url)
scoreList = json.loads(scores.text)
games = scoreList['events']
for arg in sys.argv:
    for i in range(len(games)):
        if arg in games[i]['name']:
            print(games[i]['competitions'][0]['competitors'][0]['team']['displayName'], end=' ')
            print(games[i]['competitions'][0]['competitors'][0]['score'])
            print(games[i]['competitions'][0]['competitors'][1]['team']['displayName'], end=' ')
            print(games[i]['competitions'][0]['competitors'][1]['score'])
            print(games[i]['status']['type']['detail'])
            if games[i]['status']['type']['name'] == "STATUS_IN_PROGRESS":
                try:
                    if games[i]['competitions'][0]['situation']['possession'] == games[i]['competitions'][0]['competitors'][0]['id']:
                        print(games[i]['competitions'][0]['competitors'][0]['team']['abbreviation'], end=' ')
                    else:
                        print(games[i]['competitions'][0]['competitors'][1]['team']['abbreviation'], end=' ')
                    print(games[i]['competitions'][0]['situation']['downDistanceText'])
                except KeyError:
                    print()
                    continue
            print()
