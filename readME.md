These scripts return the current score, status, and situation of requested football games.

There are seperate scripts for NFL games and CFB games.

To run, enter in the command line "python scores.py [week number] [names]". Week number is optional, but past or future weeks can be entered.  If no week number is given, the current week will be used. Names can be one or more arguments. The script will find any games that contain the given word or words. To get all scores use " " as the name. 

Example: 'python scoresCFB.py "Utah State" Boise Sooners'
This will return the Utah State Aggies', Boise State Broncos', and the Oklahoma Sooners' scores.

Example: 'python scoresNFL.py 5 Denver Chiefs"
This will return the Denver Broncos' and Kansas City Chiefs' scores from week 5.
